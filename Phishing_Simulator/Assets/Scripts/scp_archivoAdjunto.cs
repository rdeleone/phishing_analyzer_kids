using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class scp_archivoAdjunto : IPointerEnterhandler, IPointerExitHandler
{
    [SerializeField] private ParticleSystem Part_Adjunto;
    public void OnPointerEnter(PointerEventData eventData)
    {
        Part_Adjunto.Play();
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Part_Adjunto.Stop();
    }
}

