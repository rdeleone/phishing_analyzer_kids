using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class scp_correo : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField]
    private TMP_Text texto;
    public Image contenedor;

    private void Awake()
    {
        texto = GetComponentInChildren<TMP_Text>();
        contenedor = GetComponent<Image>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        texto.color = Color.black;
    }
}
