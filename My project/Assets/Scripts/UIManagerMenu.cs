using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[System.Serializable]

public class UIManagerMenu : GameManager
{
    
    [SerializeField]
    private UIDocument m_uiDocument;
    private VisualElement m_visualElement;
    private Button btnComenzarJuego;
    private Button btnCargar;
    private Button btnIniciarSesion;
    new scp_Juggador jugador;
    

    void Start()
    {

        m_visualElement = m_uiDocument.rootVisualElement;
        VisualElement menuIngresarDatos = m_visualElement.Q<VisualElement>("MenuIngresarDatos");
        menuIngresarDatos.visible = false;

        TextField txtfNombre = m_visualElement.Q<TextField>("txtfNombre");
        TextField txtfApellido = m_visualElement.Q<TextField>("txtfApellido");
        TextField txtfUsuario = m_visualElement.Q<TextField>("txtfUsuario");

        jugador = gameObject.AddComponent<scp_Juggador>();
        jugador.Nombre = txtfNombre.ToString();
        jugador.Apellido = txtfApellido.ToString();
        jugador.Usuario = txtfUsuario.ToString();
        
        btnComenzarJuego = new Button();
        btnComenzarJuego = m_visualElement.Q<Button>("btnJugar");
        btnComenzarJuego.clicked += () => { menuIngresarDatos.visible = true; Debug.Log("Estoy presienando el boton"); };

        btnCargar = new Button();
        btnCargar = m_visualElement.Q<Button>("btnCargar");
        btnCargar.clicked += () => { CargarDatosJugador(); };

        // Guardar los datos del jugador
        btnIniciarSesion = new Button();
        btnIniciarSesion = m_visualElement.Q<Button>("btnIniciarSesion");
        btnIniciarSesion.clicked += () => { GuardarDatos(jugador); CargarEscena(); };
        
    }
    public void CargarEscena()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }
    public void GuardarDatos(scp_Juggador jugador)
    {
        // Convertir el objeto DatosJugador a formato JSON
        string json = JsonUtility.ToJson(jugador);

        // Guardar los datos en PlayerPrefs
        PlayerPrefs.SetString("datosJugador", json);

        Debug.Log("Datos del jugador guardados.");
    }


    void CargarDatosJugador()
    {
        if (PlayerPrefs.HasKey("datosJugador"))
        {
            // Obtener el JSON almacenado en PlayerPrefs
            string json = PlayerPrefs.GetString("datosJugador");

            // Convertir el JSON a un objeto scp_Juggador
            jugador = JsonUtility.FromJson<scp_Juggador>(json);
        }
        else
        {
            Debug.Log("No se encontraron datos del jugador.");
        }
    }
}
