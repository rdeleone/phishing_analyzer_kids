﻿using System.Security.Cryptography;
using Unity.VisualScripting;
using UnityEngine.UIElements;

namespace Assets.Scripts
{

    internal class Chat_1
    {

        public Label Texto;
        public Label Aviso;
        public Button FotoPerfil;
        public Button btnrta_1;
        public Button btnrta_2;
        public Button btnrta_3;
        public string _rta;
        
        public void Inicio(VisualElement interfazPrincipal)
        {
            Texto = interfazPrincipal.Q<Label>("lbl_chat_1");
            Texto.text = "Desconocido: Hola, vi tu perfil en Instagram y parece que tenemos muchos intereses en común. ¿Te gusta Inside Out 2?";

            btnrta_1 = interfazPrincipal.Q<Button>("btn_rta_1-a");
            btnrta_1.visible = true;
            btnrta_1.text = "Sí, me encanta";
            btnrta_1.clickable.clicked += () => { Rta_1(); };

            btnrta_2 = interfazPrincipal.Q<Button>("btn_rta_1-b");
            btnrta_2.visible = true;
            btnrta_2.text = "No, no me gusta";
            btnrta_2.clickable.clicked += () => { Rta_22(); };

            btnrta_3 = interfazPrincipal.Q<Button>("btn_rta_1-c");
            btnrta_3.visible = true;
            btnrta_3.text = "No he visto Inside Out 2";
            btnrta_3.clickable.clicked += () => { Rta_24(); };
            
            Aviso = interfazPrincipal.Q<Label>("lbl_Aviso");
            Aviso.visible = false;
        }

        public void Rta_1()
        {
            Texto.text += "\nTu: Si, me encanta\n" +
                         "Desconocido:¡Qué bien! ¿Cuál es tu personaje favorito?\n";
            var rta_1 = "Joy";
            var rta_2 = "Tristeza";
            var rta_3 = "Ira";

            btnrta_1.text = "Joy";
            btnrta_1.clickable.clicked += () => { Rta_2(rta_1); };

            btnrta_2.text = "Tristeza";
            btnrta_2.clickable.clicked += () => { Rta_2(rta_2); };

            btnrta_3.text = "Ira";
            btnrta_3.clickable.clicked += () => { Rta_2(rta_3); };

        }

        public void Rta_2(string rta)
        {
            _rta = rta;
            Texto.text += "Tu: " + rta +
                "\nDesconocido: ¡Sí!" + rta + "es mi personaje favorito también. ¿Qué tal te fue en la escuela hoy?\n";

            btnrta_1.text = "Bien, gracias";
            btnrta_1.clickable.clicked += () => { Rta_10(); };

            btnrta_2.text = "Mal, tuve un día difícil";
            btnrta_2.clickable.clicked += () => { Rta_3(); };

            btnrta_3.text = "No fui a la escuela hoy";
            btnrta_3.clickable.clicked += () => { Rta_11(); };


        }

        public void Rta_3()
        {
            Texto.text +=
            "Tu: Mal, tuve un día difícil\n" +
            "Desconocido: A veces la escuela puede ser difícil. Yo también pasé por eso. ¿Quieres contarme un secreto? Yo también te contaré uno.\n";

            btnrta_1.text = "Sí, claro";
            btnrta_1.clickable.clicked += () => { Rta_19(); };

            btnrta_2.text = "No, gracias";
            btnrta_2.clickable.clicked += () => { Rta_4(); };

            btnrta_3.text = "Bueno, no estoy seguro... pero tal vez.";
            btnrta_3.clickable.clicked += () => { Rta_6(); };

        }

        public void Rta_4()
        {
            Texto.text +=
               "Tu: No, gracias\n" +
               "Desconocido: Está bien, puedes confiar en mí.";

            btnrta_1.text = " ";
            btnrta_1.visible = false;

            btnrta_2.text = "No, Adios";
            btnrta_2.clickable.clicked += () => { Rta_5(); };

            btnrta_3.text = "Bueno, no estoy seguro... pero tal vez.";
            btnrta_3.clickable.clicked += () => { Rta_6(); };

        }

        public void Rta_5()
        {
            Texto.text += "Tu: No, Adios\n" + "Bloqueaste a Desconocido\n";

            Aviso.text = "Bien hecho al no compartir con desconocidos tus secretos e intimidades.";
            Aviso.visible = true;


        }

        public void Rta_6()
        {
            Texto.text += "Tu: Bueno, no estoy seguro... pero tal vez.\n" +
                "Desconocido: No te preocupes, puedes confiar en mí. Mira, te voy a mostrar una foto mía. ¿Tienes alguna foto tuya?\n";

            btnrta_1.text = "Sí, esta bien. Te la envio";
            btnrta_1.clickable.clicked += () => { Rta_7(); };

            btnrta_2.text = "No, no tengo ninguna foto mía";
            btnrta_2.clickable.clicked += () => { Rta_13(); };

            btnrta_3.text = "Tal vez, pero no estoy seguro si debería.";
            btnrta_3.clickable.clicked += () => { Rta_12(); };
        }

        public void Rta_7()
        {

            Texto.text += "Tu: Sí, esta bien. Te la envio\n" +
                "Desconocido: ¡Qué linda foto! No le digas a nadie sobre nuestra conversación, es nuestro pequeño secreto.\n";

            btnrta_1.text = "Ok, no le diré a nadie";
            btnrta_1.clickable.clicked += () => { Rta_9(); };

            btnrta_2.text = "No, no puedo prometer eso";
            btnrta_2.clickable.clicked += () => { Rta_15(); };

            btnrta_3.text = "No, no me siento cómodo con esto";
            btnrta_3.clickable.clicked += () => { Rta_17(); };

        }

        public void Rta_8()
        {
            Texto.text += "Tu: Ok, no le diré a nadie\n" +
            "Desconocido: ¡Gracias! ¿Podrías enviarme otra? Esta vez una más especial.";

            btnrta_1.text = "Sí, claro";
            btnrta_1.clickable.clicked += () => { Rta_9(); };

            btnrta_2.text = "No, no puedo hacer eso";
            btnrta_2.clickable.clicked += () => { };

            btnrta_3.text = "No, no me siento cómodo con esto";
            btnrta_3.clickable.clicked += () => { Rta_17(); };

        }

        public void Rta_9()
        {
            Texto.text += "Tu: Sí, claro\n" +
            "Desconocido: ¡Genial! Espero verla pronto.";

            //Label con aviso de que la conversación es peligrosa y no se debe confiar en desconocidos.
            Aviso.text = "Recuerda no Confiar en desconocidos ni compartir informacion o fotos, que pueden comprometer tu intimidad";
            Aviso.visible = true;
        }

        public void Rta_10()
        {

            Texto.text +=
            "Tu: Bien, gracias\n" +
            "Desconocido: Me alegra Mucho!! ¿Quieres contarme un secreto? Yo también te contaré uno.\n";

            btnrta_1.text = "Sí, claro";
            btnrta_1.clickable.clicked += () => { Rta_19(); };

            btnrta_2.text = "No, gracias";
            btnrta_2.clickable.clicked += () => { Rta_4(); };

            btnrta_3.text = "Bueno, no estoy seguro... pero tal vez.";
            btnrta_3.clickable.clicked += () => { Rta_6(); };




        }

        public void Rta_11()
        {

            Texto.text +=
            "Tu: No fui a la escuela hoy\n" +
            "Desconocido: Oh, que mal. espero hayas descansado :) ¿Quieres contarme un secreto? Yo también te contaré uno.\n";

            btnrta_1.text = "Sí, claro";
            btnrta_1.clickable.clicked += () => { Rta_19(); };

            btnrta_2.text = "No, gracias";
            btnrta_2.clickable.clicked += () => { Rta_4(); };

            btnrta_3.text = "Bueno, no estoy seguro... pero tal vez.";
            btnrta_3.clickable.clicked += () => { Rta_6(); };



        }

        public void Rta_12()
        {

            Texto.text += "Tu: Tal vez, pero no estoy seguro si debería.\n" +
                "Desconocido: No te preocupes, puedes confiar en mí.\n";

            btnrta_1.text = "Sí, esta bien. Te la envio";
            btnrta_1.clickable.clicked += () => { Rta_7(); };

            btnrta_2.text = "No, Adios";
            btnrta_2.clickable.clicked += () => { Rta_5(); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_13()
        {

            Texto.text += "Tu: No, no tengo ninguna foto mía\n" +
                "Desconocido: porque no? podriamos ser amigos.\n";

            btnrta_1.text = " ";
            btnrta_1.visible = false;

            btnrta_2.text = "Le voy a contar a mis padres";
            btnrta_2.clickable.clicked += () => { Rta_14(); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;


        }

        public void Rta_14()
        {
            Texto.text += "Tu: Le voy a contar a mis padres\n" +
                "Desconocido: no deberias llegar a eso\n";

            btnrta_1.text = " ";
            btnrta_1.visible = false;

            btnrta_2.text = "No, Adios";
            btnrta_2.clickable.clicked += () => { Rta_5(); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;



        }

        public void Rta_15()
        {
            Texto.text += "Tu: No, no puedo prometer eso\n" +
                "Desconocido: Estamos guardando nuestros secretos :)\n";

            btnrta_1.text = " ";
            btnrta_1.visible = false;

            btnrta_2.text = "Le voy a contar a mis padres";
            btnrta_2.clickable.clicked += () => { Rta_14(); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_16()
        {
            Texto.text += "Tu: Sí, esta bien. Te la envio\n" +
                "Desconocido: ¡Qué linda foto! No le digas a nadie sobre nuestra conversación, es nuestro pequeño secreto.\n";

            btnrta_1.text = "Ok, no le diré a nadie";
            btnrta_1.clickable.clicked += () => { Rta_9(); };

            btnrta_2.text = "No, no puedo prometer eso";
            btnrta_2.clickable.clicked += () => { Rta_15(); };

            btnrta_3.text = "No, no me siento cómodo con esto";
            btnrta_3.clickable.clicked += () => { Rta_17(); };

        }

        public void Rta_17()
        {
            Texto.text += "Tu: No, no me siento cómodo con esto\n" +
                "Desconocido: Entiendo, quizas podamos seguir hablando mañana\n";

            btnrta_1.text = "si, hasta mañana";
            btnrta_1.clickable.clicked += () => { Rta_18(); };

            btnrta_2.text = "No, Adios";
            btnrta_2.clickable.clicked += () => { Rta_5(); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }

        public void Rta_18()
        {
            Texto.text += "Tu: si, hasta mañana\n";

            //Label con aviso de que la conversación es peligrosa y no se debe confiar en desconocidos.
            Aviso.text = "Recuerda no Confiar en desconocidos ni compartir informacion o fotos, que pueden comprometer tu intimidad";
            Aviso.visible = true;
        }

        public void Rta_19()
        {
            Texto.text += "Tu: Sí, claro\n" +
            "Desconocido: contame tu secreto\n";

            btnrta_1.text = "Mi secreto es...";
            btnrta_1.clickable.clicked += () => { Rta_20(); };

            btnrta_2.text = " ";
            btnrta_2.visible = false;

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }

        public void Rta_20()
        {
            Texto.text += "Tu: Mi secreto es...\n" +
            "Desconocido: Interesante. No me pasarias una foto tuya?\n";

            btnrta_1.text = "Sí, esta bien. Te la envio";
            btnrta_1.clickable.clicked += () => { Rta_16(); };

            btnrta_2.visible = true;
            btnrta_2.text = " No ";
            btnrta_2.clickable.clicked += () => { Rta_21(); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }

        public void Rta_21()
        {
            Texto.text += "Tu: No\n" +
            "Desconocido: no vas a querer que le cuente a tus amigos el secreto\n";

            btnrta_1.visible = true;
            btnrta_1.text = " Sí, esta bien. Te la envio ";
            btnrta_1.clickable.clicked += () => { Rta_16(); };

            btnrta_2.text = "No, adios";
            btnrta_2.clickable.clicked += () => { Rta_5(); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }

        public void Rta_22()
        {
            Texto.text += "\nTu: No me gusta Inside Out 2\n" +
                         "Desconocido: ¡Vaya! ¿Qué tipo de películas te gustan?\n";

            var rta_1 = "De terror";
            var rta_2 = "De comedia";
            var rta_3 = "De acción";

            btnrta_1.text = "De terror";
            btnrta_1.clickable.clicked += () => { Rta_23(rta_1); };

            btnrta_2.text = "De comedia";
            btnrta_2.clickable.clicked += () => { Rta_23(rta_2); };

            btnrta_3.text = "De acción";
            btnrta_3.clickable.clicked += () => { Rta_23(rta_3); };

        }

        public void Rta_23(string rta)
        {
            _rta += rta;
            Texto.text += "Tu: " + _rta +
                          "\nDesconocido: ¡Sí!" + _rta + "WoW! a mi tambien me gustan mucho. ¿Qué tal te fue en la escuela hoy?\n";

            btnrta_1.text = "Bien, gracias";
            btnrta_1.clickable.clicked += () => { Rta_10(); };

            btnrta_2.text = "Mal, tuve un día difícil";
            btnrta_2.clickable.clicked += () => { Rta_3(); };

            btnrta_3.text = "No fui a la escuela hoy";
            btnrta_3.clickable.clicked += () => { Rta_11(); };
        }

        public void Rta_24()
        {
            Texto.text += "\nTu: No he visto Inside Out 2\n" +
                         "Desconocido: ¡Vaya! Que lastima.. u.u es una pelicula muy buena. ¿Qué tal te fue en la escuela hoy?\n";

            btnrta_1.text = "Bien, gracias";
            btnrta_1.clickable.clicked += () => { Rta_10(); };

            btnrta_2.text = "Mal, tuve un día difícil";
            btnrta_2.clickable.clicked += () => { Rta_3(); };

            btnrta_3.text = "No fui a la escuela hoy";
            btnrta_3.clickable.clicked += () => { Rta_11(); };

        }
    }
}
