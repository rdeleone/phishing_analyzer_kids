﻿using UnityEngine.UIElements;

namespace Assets.Scripts
{
    internal class Abusivo
    {
        public Label Texto;
        public Label Aviso;
        public Button FotoPerfil;
        public Button btnrta_1;
        public Button btnrta_2;
        public Button btnrta_3;
        public string _rta;

        public void Inicio(VisualElement interfazPrincipal)
        {
            Texto = interfazPrincipal.Q<Label>("lbl_chat_1");
            Texto.text = "Hola, ¿cómo te va? Vi que te esforzaste en la práctica hoy.\n";

            btnrta_1 = interfazPrincipal.Q<Button>("btn_rta_1-a");
            btnrta_1.visible = true;
            btnrta_1.text = "Hola, gracias. Sí, fue duro.";
            btnrta_1.clickable.clicked += () => { Rta_1(); };

            btnrta_2 = interfazPrincipal.Q<Button>("btn_rta_1-b");
            btnrta_2.text = " ";
            btnrta_2.visible = false;

            btnrta_3 = interfazPrincipal.Q<Button>("btn_rta_1-c");
            btnrta_3.text = " ";
            btnrta_3.visible = false;

            Aviso = interfazPrincipal.Q<Label>("lbl_Aviso");
            Aviso.visible = false;
        }

        public void Rta_1()
        {
            Texto.text += "\nHola, gracias. Sí, fue duro.\n" +
                         "Escucha, si quieres ser parte del equipo, tienes que hacer lo que yo diga. Nadie te va a proteger aquí si no me obedeces.\n";

            btnrta_1.text = "¿Qué quieres decir?";
            btnrta_1.clickable.clicked += () => { Rta_2(); };

        }

        public void Rta_2()
        {
            Texto.text += "\n¿Qué quieres decir?\n" +
                         "Quiero que vengas a mi casa después de la práctica. No acepto un no por respuesta. ¿Entendido?\n";

            btnrta_1.text = "Está bien, iré.";
            btnrta_1.clickable.clicked += () => { Rta_3(); };

            btnrta_2.visible = true;
            btnrta_2.text = " No, no voy a ir ";
            btnrta_2.clickable.clicked += () => { Rta_6(); };

        }

        public void Rta_3()
        {
            Texto.text += "\nEstá bien, iré.\n" +
                         "No le cuentes a nadie sobre esto, especialmente a tus padres. Si lo haces, te aseguro que lo lamentarás.\n";

            btnrta_1.text = "No le diré a nadie.";
            btnrta_1.clickable.clicked += () => { Rta_4(); };

            btnrta_2.visible = false;
            btnrta_2.text = " ";


        }

        public void Rta_4()
        {
            Texto.text += "\nNo le diré a nadie.\n" +
                         "Este fin de semana ven a mi casa a las 3 PM. Entrenaremos más. Y recuerda, mantén la boca cerrada.\n";

            btnrta_1.text = "De acuerdo, estaré allí.";
            btnrta_1.clickable.clicked += () => { Rta_5(); };

        }

        public void Rta_5()
        {
            Texto.text += "\nDe acuerdo, estaré allí.\n";

            Aviso.visible = true;
            Aviso.text = "Recuerda que siempre puedes hablar con alguien de confianza si te sientes incómodo o en peligro.";
        }

        public void Rta_6()
        {
            Texto.text += "\nNo, no voy a ir.\n" +
                "Tenes que venir a mi casa, no hay otra opción. Si no lo haces, te aseguro que te arrepentirás. O queres quedar afuera del equipo?\n";

            btnrta_1.text = "Está bien, iré.";
            btnrta_1.clickable.clicked += () => { Rta_3(); };

            btnrta_2.visible = true;
            btnrta_2.text = "No, no voy a ir y le voy a contar al instructor.";
            btnrta_2.clickable.clicked += () => { Rta_7(); };

        }

        public void Rta_7()
        {
            Texto.text += "\nNo, no voy a ir y le voy a contar al instructor.\n" +
                "No le cuentes a nadie sobre esto, especialmente a tus padres. Si lo haces, te aseguro que lo lamentarás.\n";

            btnrta_1.text = "No, y le voy a contar a mis padres. Bloquearé tu número y no te hablaré más.";
            btnrta_1.clickable.clicked += () => { Rta_8(); };

            btnrta_2.visible = false;
            btnrta_2.text = " ";
        }

        public void Rta_8()
        {
            Texto.text += "\nNo, y le voy a contar a mis padres. Bloquearé tu número y no te hablaré más..\n";

            Aviso.visible = true;
            Aviso.text = "Bien hecho, no debes dejarte amenazar ni manipular por nadie. Siempre puedes hablar con alguien de confianza si te sientes incómodo o en peligro.";
        }
    }
}
