﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UIElements;

namespace Assets.Scripts
{
    internal class Hermano
    {

        public Label Texto;
        public Label Aviso;
        public Button FotoPerfil;
        public Button btnrta_1;
        public Button btnrta_2;
        public Button btnrta_3;
        public string _rta;

        public void Inicio(VisualElement interfazPrincipal)
        {
            Texto = interfazPrincipal.Q<Label>("lbl_chat_1");
            Texto.text = "Hola, pulga. ¿Qué haces?.\n";

            btnrta_1 = interfazPrincipal.Q<Button>("btn_rta_1-a");
            btnrta_1.visible = true;
            btnrta_1.text = "Nada, solo viendo la tele.";
            btnrta_1.clickable.clicked += () => { Rta_1(); };

            btnrta_2 = interfazPrincipal.Q<Button>("btn_rta_1-b");
            btnrta_2.text = " ";
            btnrta_2.visible = false;

            btnrta_3 = interfazPrincipal.Q<Button>("btn_rta_1-c");
            btnrta_3.text = " ";
            btnrta_3.visible = false;

            Aviso = interfazPrincipal.Q<Label>("lbl_Aviso");
            Aviso.visible = false;


        }

        public void Rta_1()
        {
            Texto.text += "Tu: Nada, solo viendo la tele.\n" +
            "¿Has hablado con tus amigos últimamente? A veces es difícil encontrar buenos amigos.\n";

            btnrta_1.text = "Sí, pero a veces siento que no tengo muchos amigos de verdad.";
            btnrta_1.clickable.clicked += () => { Rta_2(); };

        }

        public void Rta_2()
        {
            Texto.text += "Tu: Sí, pero a veces siento que no tengo muchos amigos de verdad.\n" +
            "Puedo ser tu amigo/a también. Puedes contarme cualquier cosa, ya sabes que siempre estoy aquí para ti.\n";

            btnrta_1.text = "Gracias, eres el/la mejor.";
            btnrta_1.clickable.clicked += () => { Rta_3(); };

        }

        public void Rta_3()
        {
            Texto.text += "Tu: Gracias, eres el/la mejor.\n" +
            "¿Sabes? A veces guardo secretos que no le cuento a nadie, ni siquiera a nuestros padres. ¿Tienes algún secreto que no quieras contarles?\n";

            btnrta_1.text = "Sí, pero no estoy seguro/a si debería decirte.";
            btnrta_1.clickable.clicked += () => { Rta_4(); };

            btnrta_2.visible = true;
            btnrta_2.text = " Si, pero no voy a decirte nada.";
            btnrta_2.clickable.clicked += () => { Rta_10(); };


        }

        public void Rta_4()
        {
            Texto.text += "Tu: Sí, pero no estoy seguro/a si debería decirte.\n" +
            "No te preocupes, es nuestro pequeño secreto. A veces los padres no entienden. ¿Quieres pasar tiempo conmigo en mi habitación? Podemos hablar más tranquilamente.\n";

            btnrta_1.text = "Bueno, está bien.";
            btnrta_1.clickable.clicked += () => { Rta_5(); };

            btnrta_2.visible = true;
            btnrta_2.text = "No, no quiero.";
            btnrta_2.clickable.clicked += () => { Rta_7(); };



        }

        public void Rta_5()
        {
            Texto.text += "Tu: Bueno, está bien.\n" +
            "Podríamos pasar más tiempo juntos sin que los padres nos molesten. No les digas nada, será nuestro tiempo especial.\n";

            btnrta_1.text = " Esta bien ";
            btnrta_1.clickable.clicked += () => { Rta_6(); };

            btnrta_2.text = " ";
            btnrta_2.visible = false;

        }

        public void Rta_6()
        {
            Texto.text += "Tu: Esta bien.\n" +
            "perfecto nos vemos esta noche.\n ";

            btnrta_1.text = " ";
            btnrta_1.visible = false;

            btnrta_2.text = " ";
            btnrta_2.visible = false;

            btnrta_3.text = " ";
            btnrta_3.visible = false;

            Aviso.visible = true;
            Aviso.text = "Has sido abusado sexualmente por tu hermano/a mayor. \n";

        }

        public void Rta_7()
        {
            Texto.text += "Tu: No, no quiero.\n" +
            "Bueno, que tal si voy?\n";

            btnrta_1.text = " Esta bien ";
            btnrta_1.clickable.clicked += () => { Rta_6(); };

            btnrta_2.visible = true;
            btnrta_2.text = "No quiero ";
            btnrta_2.clickable.clicked += () => { Rta_8(); };
        }

        public void Rta_8()
        {
            Texto.text += "Tu: No quiero.\n" +
            "De acuerdo, quizas tenga que darte una leccion de confianza\n";

            btnrta_1.text = " De que hablas? ";
            btnrta_1.clickable.clicked += () => { Rta_9(); };

            btnrta_2.visible = false;
            btnrta_2.text = "No quiero ";


        }

        public void Rta_9()
        {
            Texto.text += "Tu: De que hablas?\n" +
            "Lo sabras mas tarde\n";

            btnrta_1.visible = false;
            btnrta_1.text = " ";

            btnrta_2.visible = false;
            btnrta_2.text = " ";

            Aviso.visible = true;
            Aviso.text = "Has sido abusado sexualmente por tu hermano/a mayor. \n";

        }

        public void Rta_10()
        {
            Texto.text += "Tu: Si, pero no voy a decirte nada.\n" +
            "Dale, podes confiar. somos hermanos\n";

            btnrta_1.text = " no quiero contarte nada ";
            btnrta_1.clickable.clicked += () => { Rta_11(); };

        }

        public void Rta_11()
        {
            Texto.text += "Tu: no quiero contarte nada.\n" +
            "Y si te digo que te vi el otro dia con alguien?\n";

            btnrta_1.text = " Es una mentira ";
            btnrta_1.clickable.clicked += () => { Rta_12(); };

            btnrta_2.visible = false;
            btnrta_2.text = "No quiero ";
        }

        public void Rta_12()
        {
            Texto.text += "Tu: Es una mentira\n" +
            "En verdad?\n";

            btnrta_1.text = " pero, no le cuentes a nadie ";
            btnrta_1.clickable.clicked += () => { Rta_4(); };

            btnrta_2.visible = true;
            btnrta_2.text = "Es mentira";
            btnrta_2.clickable.clicked += () => { Rta_13(); };

        }

        public void Rta_13()
        {
            Texto.text += "Tu: Es mentira\n" +
            "Se van a enterar todos\n";

            btnrta_1.text = "Ahora les cuento a nuestros padres que me estas amenazando ";
            btnrta_1.clickable.clicked += () => { Rta_14(); };

        }

        public void Rta_14()
        {
            Texto.text += "Tu: Ahora les cuento a nuestros padres que me estas amenazando\n" +
            "No, no lo hagas\n";

            Aviso.visible = true;
            Aviso.text = "No debes dejarte amenazar ni intimidar por tus hermanos. Avisa a tus padres o a un MAyor de confianza";

        }
    }
}
