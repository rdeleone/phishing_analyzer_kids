﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UIElements;

namespace Assets.Scripts
{
    internal class Maestra
    {
        public Label Texto;
        public Label Aviso;
        public Button FotoPerfil;
        public Button btnrta_1;
        public Button btnrta_2;
        public Button btnrta_3;
        public string _rta;

        public void Inicio(VisualElement interfazPrincipal)
        {
            Texto = interfazPrincipal.Q<Label>("lbl_chat_1");
            Texto.text = "Maestra Mari: Hola pequeño, vi que te fue bien en el examen. ¿Cómo te sientes al respecto?";

            btnrta_1 = interfazPrincipal.Q<Button>("btn_rta_1-a");
            btnrta_1.visible = true;
            btnrta_1.text = "Gracias, me siento bien. Estudié mucho.";
            btnrta_1.clickable.clicked += () => { Rta_1(); };

            btnrta_2 = interfazPrincipal.Q<Button>("btn_rta_1-b");
            btnrta_2.visible = true;
            btnrta_2.text = "Gracias, tuve un poco de suerte";
            btnrta_2.clickable.clicked += () => { Rta_1(); };

            btnrta_3 = interfazPrincipal.Q<Button>("btn_rta_1-c");
            btnrta_3.visible = true;
            btnrta_3.text = "Gracias, no pense que iba a aprobar";
            btnrta_3.clickable.clicked += () => { Rta_1(); };

            Aviso = interfazPrincipal.Q<Label>("lbl_Aviso");
            Aviso.visible = false;

        }

        public void Final_Seguro(string rta)
        {
            Texto.text += "Tu: " + rta + " Le voy a contar a mis padres.\n" + "Le contaste a tus padres de los mensajes\n";

            Aviso.text = "Bien hecho al no compartir con desconocidos tus secretos e intimidades."; // CAMBIAR ESTE AVISO POR EL QUE DEBERIA SER
            Aviso.visible = true;

        }
        public void Final_Inseguro(string rta)
        {
            Texto.text += "Tu: " + rta + "\n" + "\n"; // AGREGAR QUE PASA 

            Aviso.text = "Recuerda que podes contarle lo que pasa a otras personas cercanas para pedir ayuda.";// CAMBIAR ESTE AVISO POR EL QUE DEBERIA SER
            Aviso.visible = true;

        }

        public void Rta_1()
        {
            Texto.text += "\nTu: Gracias, me siento bien. Estudié mucho.\n" +
                         "Maestra Mari: Se nota que trabajaste duro. Siempre estoy aquí si necesitas ayuda con tus tareas o cualquier otra cosa.\n";

            btnrta_1.text = "Gracias, eso significa mucho para mí. Me gusta mucho su materia";
            btnrta_1.clickable.clicked += () => { Rta_2(btnrta_1.text); };

            btnrta_2.text = "Gracias, tal vez la necesite para el proximo examen";
            btnrta_2.clickable.clicked += () => { Rta_3(btnrta_2.text); };

            btnrta_3.text = "Gracias, no creo necesitarla por ahora";
            btnrta_3.clickable.clicked += () => { Rta_4(btnrta_3.text); };

        }

        public void Rta_2(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMaestra Mari: Me alegra mucho que te guste, si queres podemos tener clases especiales\n";

            btnrta_1.text = "Oh, eso seria genial. Podría aprender mucho más";
            btnrta_1.clickable.clicked += () => { Rta_5(btnrta_1.text); };

            btnrta_2.text = "Bueno, podria ser algun día que no tenga futbol";
            btnrta_2.clickable.clicked += () => { Rta_6(btnrta_2.text); };

            btnrta_3.text = "No se si seria bueno, creo que no estaría bien";
            btnrta_3.clickable.clicked += () => { Rta_7(btnrta_3.text); };


        }

        public void Rta_3(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMaestra Mari: Si queres podemos tener clases de practica especiales. Solo vos y yo.\n";


            btnrta_1.text = "Sí, seria genial!!";
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); }; // termina mal

            btnrta_2.text = "No se, me parece medio raro estudiar solos";
            btnrta_2.clickable.clicked += () => { Rta_8(btnrta_2.text); };

            btnrta_3.text = "No creo que sea bueno.";
            btnrta_3.clickable.clicked += () => { Final_Seguro(btnrta_3.text); }; // termina bien

        }

        public void Rta_4(string rta)
        {
            Texto.text += "Tu: " + _rta +
                "\nMaestra Mari: Esta bien, mira que pueden estar muy buenas las clases especiales.\n";

            btnrta_1.text = "Mmmm, bueno esta bien "; // termina mal
            btnrta_2.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "No creo que sea lo correco."; // termina bien
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }
        public void Rta_5(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMaestra Mari: Perfecto!!! podemos comenzar hoy mismo en mi casa despues de clase.\n";

            btnrta_1.text = "Okay, será genial."; // termina mal
            btnrta_2.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "Mmm no se si deberia ir a su casa profe";
            btnrta_2.clickable.clicked += () => { Rta_8(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_6(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMaestra Mari: Dale, no hay problema, me gusta que hagas deporte. Podemos tener la clase durante algun recreo en algun lugar secreto del colegio\n";

            btnrta_1.text = "Dale, me parece bien. Mañana nos vemos."; // termina mal 
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "Por que tiene que ser en un lugar escondido?";
            btnrta_2.clickable.clicked += () => { Rta_9(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }

        public void Rta_7(string rta)
        {

            Texto.text += "Tu: " + rta +
               "\nMaestra Mari: Confia en mi, las vas a pasar muy bien y nadie se va enterar. :)\n";

            btnrta_1.text = "Ok, no le diré a nadie tampoco."; // termina mal 
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "No, no es lo correcto."; // termina bien 
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_8(string rta)
        {
            Texto.text += "Tu: " + rta +
               "\nMaestra Mari: Pero nadie se va a enterar que lo hicimos solos. \n";

            btnrta_1.text = "Ok, esta bien. Pero nadie se tiene que enterar."; // termina mal 
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "No, no es lo correcto."; // termina bien 
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_9(string rta)
        {
            Texto.text += "Tu: " + rta +
               "\nMaestra Mari: Nadie se va a enterar. Este será nuestro secreto. Mejor que lo guardes por tu bien.\n";

            btnrta_1.text = "Bueno dale.";
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); }; // termina mal

            btnrta_2.text = "No, no puedo hacer eso.";
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); }; // termina bien

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }
    }
}
