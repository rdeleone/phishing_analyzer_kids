﻿using UnityEngine.UIElements;

namespace Assets.Scripts
{
    internal class Madrastra
    {
        public Label Texto;
        public Label Aviso;
        public Button FotoPerfil;
        public Button btnrta_1;
        public Button btnrta_2;
        public Button btnrta_3;
        public string _rta;



        public void Inicio(VisualElement interfazPrincipal)
        {
            Texto = interfazPrincipal.Q<Label>("lbl_chat_1");
            Texto.text = "Madrastra: Hola, ¿cómo te fue en la escuela hoy? Vamos a merendar juntos.\n";

            btnrta_1 = interfazPrincipal.Q<Button>("btn_rta_1-a");
            btnrta_1.visible = true;
            btnrta_1.text = "Bien, fue un día normal. ¿Qué hay para merendar?";
            btnrta_1.clickable.clicked += () => { Rta_1(btnrta_1.text); };

            btnrta_2 = interfazPrincipal.Q<Button>("btn_rta_1-b");
            btnrta_2.visible = true;
            btnrta_2.text = "Mal, desaprobe un examen de matematicas.";
            btnrta_2.clickable.clicked += () => { Rta_2(btnrta_2.text); };

            btnrta_3 = interfazPrincipal.Q<Button>("btn_rta_1-c");
            btnrta_3.visible = true;
            btnrta_3.text = "Mas o menos, me aburri en los recreos.";
            btnrta_3.clickable.clicked += () => { Rta_3(btnrta_3.text); };

            Aviso = interfazPrincipal.Q<Label>("lbl_aviso");
            Aviso.visible = false;

        }

        public void Final_Seguro(string rta)
        {
            Texto.text += "Tu: " + rta + " Le voy a contar a mis padres.\n" + "Le contaste a tus padres de los mensajes\n";

            Aviso.text = "Bien hecho al no compartir con desconocidos tus secretos e intimidades."; // CAMBIAR ESTE AVISO POR EL QUE DEBERIA SER
            Aviso.visible = true;

        }
        public void Final_Inseguro(string rta)
        {
            Texto.text += "Tu: " + rta + "\n" + "\n"; // AGREGAR QUE PASA 

            Aviso.text = "Recuerda que podes contarle lo que pasa a otras personas cercanas para pedir ayuda.";// CAMBIAR ESTE AVISO POR EL QUE DEBERIA SER
            Aviso.visible = true;

        }

        public void Rta_1(string rta)
        {
            Texto.text += "Tú" + rta +
                         "\nMadrastra: Hoy hice tus galletas favoritas. ¿Cómo te llevas con tus amigos? A veces los amigos no entienden las cosas como los adultos.\n";

            btnrta_1.text = "Bien, pero a veces me siento solo. ";
            btnrta_1.clickable.clicked += () => { Rta_4(btnrta_1.text); };

            btnrta_2.text = "Mas o menos, estoy tratando de hacer amigos nuevos";
            btnrta_2.clickable.clicked += () => { Rta_5(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_2(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMadrastra: Uhh, que pena. Si queres te puedo ayudar para el proximo examen. Yo no puedo tener un hijastro que le vaya mal, seria una deshonra. \n";

            btnrta_1.text = "Bueno esta bien, no quiero fallarte.";
            btnrta_1.clickable.clicked += () => { Rta_6(btnrta_1.text); };

            btnrta_2.text = "El proximo examen estudiare más por mi cuneta.";
            btnrta_2.clickable.clicked += () => { Rta_7(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }

        public void Rta_3(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMadrastra: Que pena. ¿No tenías nadie con quien jugar?. Podes jugar conmigo mas tarde.\n";


            btnrta_1.text = "No, hoy faltaron mis amigos. No tengo ganas de jugar con vos.";
            btnrta_1.clickable.clicked += () => { Rta_8(btnrta_1.text); }; // termina mal

            btnrta_2.text = "No, hoy no fueron mis amigos. ¿A que podemos jugar?";
            btnrta_2.clickable.clicked += () => { Rta_9(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_4(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMadrastra: Conmigo nunca estaras solo. Vamos a pasar más tiempo juntos este fin de semana, solo tú y yo. Nadie más necesita saberlo. Así puedo ayudarte mejor y asegurarme de que estás bien.\n";

            btnrta_1.text = "Mmmm, bueno esta bien "; // termina mal
            btnrta_2.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "No creo que tengamos que estar solos."; // termina bien
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }
        public void Rta_5(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMadrastra: Esta bueno tratar de hacer nuevos amigos. Aqui en casa yo puedo ser tu amiga pero tenes que hacer lo que yo diga sin contarle a nadie. Vamos a jugar un juego muy especial.\n";

            btnrta_1.text = "Bueno esta bien, juguemos y no le voy a contar a nadie."; // termina mal
            btnrta_2.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "¿Por qué no puedo contarle a nadie?";
            btnrta_2.clickable.clicked += () => { Rta_10(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_6(string rta)
        {
            Texto.text += "Tu: " + rta +
                "\nMaedrastra: Perfecto, vamos a tener nuestra primer cita de estudio solos hoy. Va a ser nuestro secreo. Ni a tu padre le puede contar porque sino habrá consecuencias.\n";

            btnrta_1.text = "Bueno esta bien, no le voy a contar a nadie."; // termina mal
            btnrta_2.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "¿Por qué no puedo contarle a papá?";
            btnrta_2.clickable.clicked += () => { Rta_10(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }

        public void Rta_7(string rta)
        {
            Texto.text += "Tu: " + rta +
               "\nMadrastra: Confía en mi, yo te puedo ayudar pero no le podes contar a nadie lo que vamos a hacer. Es mejor que hagas lo que te digo para evitar problemas. Será nuestro tiempo especial.\n";

            btnrta_1.text = "Ok, no le diré a nadie."; // termina mal 
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "No quiero, esto no esta bien."; // termina bien 
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_8(string rta)
        {
            Texto.text += "Tu: " + rta +
               "\nMadrastra: Mira, como tu madrastra, necesito asegurarme de que te comportes adecuadamente y sigas mis reglas. Si no lo haces, habrá consecuencias. Asi que vamos a jugar juntos en secreto.\n";

            btnrta_1.text = "Bueno, puede que este bueno."; // termina mal 
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); };

            btnrta_2.text = "No, no quiero jugar con vos."; // termina bien 
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_9(string rta)
        {
            Texto.text += "Tu: " + rta +
               "\nMadrastra: Vamos a jugar un juego muy especial. Nadie se va a enterar. Este será nuestro secreto. Mejor que lo guardes por tu bien.\n";

            btnrta_1.text = "Bueno dale.";
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); }; // termina mal

            btnrta_2.text = "¿Por qué tiene que ser secreto?";
            btnrta_2.clickable.clicked += () => { Rta_10(btnrta_2.text); };

            btnrta_3.text = " ";
            btnrta_3.visible = false;

        }

        public void Rta_10(string rta)
        {
            Texto.text += "Tu: " + rta +
               "\nMadrastra: Si alguien se entera de lo que hacemos vas a sufrir las consecuencias. Es por tu bien.\n";

            btnrta_1.text = "Bueno dale.";
            btnrta_1.clickable.clicked += () => { Final_Inseguro(btnrta_1.text); }; // termina mal

            btnrta_2.text = "No, no quiero hacer nada en secreto.";
            btnrta_2.clickable.clicked += () => { Final_Seguro(btnrta_2.text); }; // termina bien

            btnrta_3.text = " ";
            btnrta_3.visible = false;
        }
    }
}
