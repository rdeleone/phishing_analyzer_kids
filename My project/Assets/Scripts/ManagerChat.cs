﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using Assets.Scripts;
using System.Net.NetworkInformation;

[System.Serializable]
public class ManagerChat : GameManager
{
    [SerializeField]
    private UIDocument m_uiDocument;
    private VisualElement interfazPrincipal;
    Label Texto;
    Button btnrta_1;
    Button btnrta_2;
    Button btnrta_3;
    Button btnrta_4;
    Button btnrta_5;

    Button btnjug_1;
    Button btnjug_2;
    Button btnjug_3;
    
    Button Cierre;
    Button abrir;

    void OnEnable()
    {
        interfazPrincipal = m_uiDocument.rootVisualElement;

        interfazPrincipal = interfazPrincipal.Q<VisualElement>("AppChat");
        interfazPrincipal.visible = false;

        Chat_1 chat_1 = new Chat_1();
        btnrta_1 = interfazPrincipal.Q<Button>("btn_Desconocido");
        btnrta_1.text = "Hola. Vi tu perfil...";
        btnrta_1.clickable.clicked += () => { chat_1.Inicio(interfazPrincipal); };

        Abusivo abusivo = new Abusivo();   
        btnrta_2 = interfazPrincipal.Q<Button>("btn_abusivo");
        btnrta_2.text = "Hola, ¿cómo te va? Vi que te esforzaste en la práctica hoy.";
        btnrta_2.clickable.clicked += () => { abusivo.Inicio(interfazPrincipal); };

        Maestra maestra = new Maestra();
        btnrta_3 = interfazPrincipal.Q<Button>("btn_maestra");
        btnrta_3.text = "Hola pequeño, vi que te fue bien en el examen.";
        btnrta_3.clickable.clicked += () => { maestra.Inicio(interfazPrincipal); };
         
        Hermano hermano = new Hermano();
        btnrta_4 = interfazPrincipal.Q<Button>("btn_hermano");
        btnrta_4.text = "Hola, pulga. ¿Qué haces?.";
        btnrta_4.clickable.clicked += () => { hermano.Inicio(interfazPrincipal); };

        Madrastra madrastra = new Madrastra();
        btnrta_5 = interfazPrincipal.Q<Button>("btn_madrastra");
        btnrta_5.text = "Hola, ¿cómo te fue en la escuela?";
        btnrta_5.clickable.clicked += () => { madrastra.Inicio(interfazPrincipal); };
    

        Texto = interfazPrincipal.Q<Label>("lbl_Aviso");
        Texto.visible = false;

        btnjug_1 = interfazPrincipal.Q<Button>("btn_rta_1-a");
        btnjug_1.visible = false;
        btnjug_1.text = " ";

        btnjug_2 = interfazPrincipal.Q<Button>("btn_rta_1-b");
        btnjug_2.visible = false;
        btnjug_2.text = " ";

        btnjug_3 = interfazPrincipal.Q<Button>("btn_rta_1-c");
        btnjug_3.visible = false;
        btnjug_3.text = " ";

        Cierre = interfazPrincipal.Q<Button>("btn_cerrar");
        Cierre.clickable.clicked += () => { cerrar(); };

    }
        
    private void cerrar()
    {
        interfazPrincipal = interfazPrincipal.Q<VisualElement>("AppChat");
        interfazPrincipal.visible = false;

        btnjug_1.visible = false;
        btnjug_2.visible = false;
        btnjug_3.visible = false;

        foreach (var item in interfazPrincipal.Children())
        {
            item.visible = false;
        }
        
    }

}

