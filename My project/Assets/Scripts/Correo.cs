﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    [System.Serializable]
    public class Correo
    {
		public int id;
		public string grupo;
		public string fase;
		public string contenido;
        public string asunto;

	}
}
