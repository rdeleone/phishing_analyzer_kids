using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Juggador : MonoBehaviour
{
    private string nombre;

    public string Nombre
    {
        get { return nombre; }
        set { nombre = value; }
    }

    private string apellido;

    public string Apellido
    {
        get { return apellido; }
        set { apellido = value; }
    }

    private string usuario;

    public string Usuario
    {
        get { return usuario; }
        set { usuario = value; }
    }

    private int intentos;

    public int Intentos
    {
        get { return intentos; }
        set { intentos = value; }
    }

}
