using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

[System.Serializable]
public class UiManager : GameManager, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField]
    private UIDocument m_uiDocument;
    private VisualElement interfazPrincipal;

    //labels
    private Label lblOrigenSospechoso;
    private Label lblLinkSospechoso;
    private Label lblFalloRepoerte;


    //botones
    private Button btn_MailBox;
    private Button btn_abrir;
    private Button btnCerrarMailBox;
    private Button btnMailAsuntoMail_1;
    private Button btnMailAsuntoMail_2;
    private Button btnMailAsuntoMail_3;
    private Button btnMailAsuntoMail_4;
    private Button btnMailAsuntoMail_5;
    private Button btnRemitenteMail_1;
    private Button btnLinkMaliciosoMail_1;
    private Button btnReporteMail_1;
    private Button btnCerrarReporte;
    private Button btnReporte_1;
    private Button btnReporte_2;
    private Button btnReporte_3;
    private Button btnCerrarFalloReporte;



    void Awake()
    {
        
        
        interfazPrincipal = m_uiDocument.rootVisualElement;

        // instancias de las ventanas
        var MailBox = interfazPrincipal.Q<VisualElement>("MailBox");
        MailBox.visible = false;

        var BandejaDeEntrada = interfazPrincipal.Q<VisualElement>("BandejaDeEntrada");
        BandejaDeEntrada.visible = false;
        
        var ventanaMail1 = interfazPrincipal.Q<VisualElement>("VentanaMail_1");
        ventanaMail1.visible = false;

        var VentanaReporte = interfazPrincipal.Q<VisualElement>("VentanaReporte");
        VentanaReporte.visible = false;
        var VentanaReporte_2 = interfazPrincipal.Q<VisualElement>("VentanaReporte_2");
        VentanaReporte.visible = false;
        var VentanaReporte_3 = interfazPrincipal.Q<VisualElement>("VentanaReporte_3");
        VentanaReporte.visible = false;
        var VentanaReporte_4 = interfazPrincipal.Q<VisualElement>("VentanaReporte_4");
        VentanaReporte.visible = false;
        var VentanaReporte_5 = interfazPrincipal.Q<VisualElement>("VentanaReporte_5");
        VentanaReporte.visible = false;


    }

    void Start()
    {
        CerrarMailBox();
        //labels
        #region labels 
        lblLinkSospechoso = interfazPrincipal.Q<Label>("lblLinkSospechoso");
        lblLinkSospechoso.visible = false;
        lblOrigenSospechoso = interfazPrincipal.Q<Label>("lblOrigenSospechoso");
        lblOrigenSospechoso.visible = false;
        lblFalloRepoerte = interfazPrincipal.Q<Label>("lblFalloRepoerte");
        lblFalloRepoerte.visible = false;

        #endregion

        // botones interfaz

        #region botones Escritorio

        btn_MailBox = interfazPrincipal.Q<Button>("btn_MailBox");
        btn_MailBox.clickable.clicked += () => { MostrarMailBox(); };

        btn_abrir = interfazPrincipal.Q<Button>("btn_abrir");
        btn_abrir.clickable.clicked += () => { AbrirChat(); };



        #endregion


        #region botones bandeja de entrada

        btnMailAsuntoMail_1 = interfazPrincipal.Q<Button>("btnMailAsuntoMail_1");
        btnMailAsuntoMail_1.text = "Confirmaci�n de compra en l�nea";
        btnMailAsuntoMail_1.clickable.clicked += () => { MostrarMail1("VentanaMail_1");  };
        
        btnCerrarMailBox = interfazPrincipal.Q<Button>("btnCerrarMailBox");
        btnCerrarMailBox.clickable.clicked += () => { CerrarMailBox(); };

        btnMailAsuntoMail_2 = interfazPrincipal.Q<Button>("btnMailAsuntoMail_2");
        btnMailAsuntoMail_2.text = "Ganador de sorteo TikTok";
        btnMailAsuntoMail_2.clickable.clicked += () => { MostrarMail1("VentanaMail_2"); };

        btnMailAsuntoMail_3 = interfazPrincipal.Q<Button>("btnMailAsuntoMail_3");
        btnMailAsuntoMail_3.text = "Ultima oportunidad para Reclamar tus puntos";
        btnMailAsuntoMail_3.clickable.clicked += () => { MostrarMail1("VentanaMail_3"); };

        btnMailAsuntoMail_4 = interfazPrincipal.Q<Button>("btnMailAsuntoMail_4");
        btnMailAsuntoMail_4.text = "Re: Tu tarea no fue recibida";
        btnMailAsuntoMail_4.clickable.clicked += () => { MostrarMail1("VentanaMail_4"); };
        
        btnMailAsuntoMail_5 = interfazPrincipal.Q<Button>("btnMailAsuntoMail_5");
        btnMailAsuntoMail_5.text = "Favor";
        btnMailAsuntoMail_5.clickable.clicked += () => { MostrarMail1("VentanaMail_5"); };

        #endregion

        #region botones ventana mail 1

        btnRemitenteMail_1 = interfazPrincipal.Q<Button>("btnRemitenteMail_1");
        btnRemitenteMail_1.clickable.clicked += () => { MostrarMensajes("lblOrigenSospechoso");  };

        btnLinkMaliciosoMail_1 = interfazPrincipal.Q<Button>("btnLinkMaliciosoMail_1");
        btnLinkMaliciosoMail_1.clickable.clicked += () => { MostrarMensajes("lblLinkSospechoso");  };

        btnReporteMail_1 = interfazPrincipal.Q<Button>("btnReporteMail_1");
        btnReporteMail_1.clickable.clicked += () => { MostrarVentana("VentanaReporte");  };

        btnCerrarReporte = interfazPrincipal.Q<Button>("btnCerrarReporte");
        btnCerrarReporte.clickable.clicked += () => { CerrarVentana("VentanaReporte"); };

        btnReporte_1 = interfazPrincipal.Q<Button>("btnReporte_1");
        btnReporte_1.clickable.clicked += () => { ReporteCorrecto("VentanaMail_1", "VentanaReporte", "btnMailAsuntoMail_1", "lblLinkSospechoso", "lblOrigenSospechoso"); };

        btnReporte_2 = interfazPrincipal.Q<Button>("btnReporte_2");
        btnReporte_2.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        btnReporte_3 = interfazPrincipal.Q<Button>("btnReporte_3");
        btnReporte_3.clickable.clicked += () => { ReporteCorrecto("VentanaMail_1", "VentanaReporte", "btnMailAsuntoMail_1", "lblLinkSospechoso", "lblOrigenSospechoso"); };

        #endregion

        #region botones ventana mail 2

        btnRemitenteMail_1 = interfazPrincipal.Q<Button>("btnRemitenteMail_2");
        btnRemitenteMail_1.clickable.clicked += () => { MostrarMensajes("lblOrigenSospechoso"); };

        btnLinkMaliciosoMail_1 = interfazPrincipal.Q<Button>("btnLinkMaliciosoMail_2");
        btnLinkMaliciosoMail_1.clickable.clicked += () => { MostrarMensajes("lblLinkSospechoso"); };

        btnReporteMail_1 = interfazPrincipal.Q<Button>("btnReporteMail_1");
        btnReporteMail_1.clickable.clicked += () => { MostrarVentana("VentanaReporte_2"); };

        btnCerrarReporte = interfazPrincipal.Q<Button>("btnCerrarReporte");
        btnCerrarReporte.clickable.clicked += () => { CerrarVentana("VentanaReporte_2"); };

        btnReporte_1 = interfazPrincipal.Q<Button>("btnReporte_1");
        btnReporte_1.clickable.clicked += () => { ReporteCorrecto("VentanaMail_2", "VentanaReporte_2", "btnMailAsuntoMail_2", "lblLinkSospechoso", "lblOrigenSospechoso"); };

        btnReporte_2 = interfazPrincipal.Q<Button>("btnReporte_2");
        btnReporte_2.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        btnReporte_3 = interfazPrincipal.Q<Button>("btnReporte_3");
        btnReporte_3.clickable.clicked += () => { ReporteCorrecto("VentanaMail_2", "VentanaReporte_2", "btnMailAsuntoMail_2", "lblLinkSospechoso", "lblOrigenSospechoso"); };

        #endregion

        #region botones ventana mail 3
        btnRemitenteMail_1 = interfazPrincipal.Q<Button>("btnRemitenteMail_3");
        btnRemitenteMail_1.clickable.clicked += () => { MostrarMensajes("lblAsuntoSospechoso"); };

        btnLinkMaliciosoMail_1 = interfazPrincipal.Q<Button>("btnQRSospechoso");
        btnLinkMaliciosoMail_1.clickable.clicked += () => { MostrarMensajes("lblQRSospechoso"); };

        btnReporteMail_1 = interfazPrincipal.Q<Button>("btnReporteMail_3");
        btnReporteMail_1.clickable.clicked += () => { MostrarVentana("VentanaReporte_3"); };

        btnCerrarReporte = interfazPrincipal.Q<Button>("btnCerrarReporte");
        btnCerrarReporte.clickable.clicked += () => { CerrarVentana("VentanaReporte_3"); };

        btnReporte_1 = interfazPrincipal.Q<Button>("btnReporte_1");
        btnReporte_1.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        btnReporte_2 = interfazPrincipal.Q<Button>("btnReporte_2");
        btnReporte_2.clickable.clicked += () => { ReporteCorrecto("VentanaMail_3", "VentanaReporte_3", "btnMailAsuntoMail_3", "lblQRSospechoso", "lblAsuntoSospechoso"); };

        btnReporte_3 = interfazPrincipal.Q<Button>("btnReporte_3");
        btnReporte_3.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        #endregion

        #region botones ventana mail 4
        btnRemitenteMail_1 = interfazPrincipal.Q<Button>("btnRemitenteMail_4");
        btnRemitenteMail_1.clickable.clicked += () => { MostrarMensajes("lblAsuntoSospechoso"); };

        btnLinkMaliciosoMail_1 = interfazPrincipal.Q<Button>("btnLinkSospechoso");
        btnLinkMaliciosoMail_1.clickable.clicked += () => { MostrarMensajes("lblLinkSospechoso"); };

        btnReporteMail_1 = interfazPrincipal.Q<Button>("btnReporteMail_4");
        btnReporteMail_1.clickable.clicked += () => { MostrarVentana("VentanaReporte_4"); };

        btnCerrarReporte = interfazPrincipal.Q<Button>("btnCerrarReporte");
        btnCerrarReporte.clickable.clicked += () => { CerrarVentana("VentanaReporte_4"); };

        btnReporte_1 = interfazPrincipal.Q<Button>("btnReporte_1");
        btnReporte_1.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        btnReporte_2 = interfazPrincipal.Q<Button>("btnReporte_2");
        btnReporte_2.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        btnReporte_3 = interfazPrincipal.Q<Button>("btnReporte_3");
        btnReporte_3.clickable.clicked += () => { ReporteCorrecto("VentanaMail_4", "VentanaReporte_4", "btnMailAsuntoMail_4", "lblAsuntoSospechoso", "lblLinkSospechoso"); };

        #endregion

        #region botones ventana mail 5
        btnRemitenteMail_1 = interfazPrincipal.Q<Button>("btnAsuntoMalMail_5");
        btnRemitenteMail_1.clickable.clicked += () => { MostrarMensajes("lblAsuntoSospechoso"); };

        btnLinkMaliciosoMail_1 = interfazPrincipal.Q<Button>("btnLinkSospechoso");
        btnLinkMaliciosoMail_1.clickable.clicked += () => { MostrarMensajes("lblLinkSospechoso"); };

        btnReporteMail_1 = interfazPrincipal.Q<Button>("btnReporteMail_5");
        btnReporteMail_1.clickable.clicked += () => { MostrarVentana("VentanaReporte_5"); };

        btnCerrarReporte = interfazPrincipal.Q<Button>("btnCerrarReporte");
        btnCerrarReporte.clickable.clicked += () => { CerrarVentana("VentanaReporte_5"); };

        btnReporte_1 = interfazPrincipal.Q<Button>("btnReporte_1");
        btnReporte_1.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        btnReporte_2 = interfazPrincipal.Q<Button>("btnReporte_2");
        btnReporte_2.clickable.clicked += () => { ReporteIncorrecto("lblFalloRepoerte"); };

        btnReporte_3 = interfazPrincipal.Q<Button>("btnReporte_3");
        btnReporte_3.clickable.clicked += () => { ReporteCorrecto("VentanaMail_5", "VentanaReporte_5", "btnMailAsuntoMail_5", "lblLinkSospechoso", "lblAsuntoSospechoso"); };

        #endregion


    }

    public void OnPointerExit(PointerLeaveEvent eventData)
    {
        var boton = eventData.currentTarget as VisualElement;
        boton.style.backgroundColor = new StyleColor(Color.white);
    }

    private void OnPointerEnter(PointerEnterEvent evt)
    {

        
    }

    private void MostrarMailBox()
    {
        interfazPrincipal.Q<VisualElement>("MailBox").visible = true;
        interfazPrincipal.Q<VisualElement>("BandejaDeEntrada").visible = true;
        interfazPrincipal.Q<VisualElement>("Botonera").visible = true;
        interfazPrincipal.Q<VisualElement>("CintaDeOpciones").visible = true;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_1").visible = true;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_2").visible = true;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_3").visible = true;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_4").visible = true;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_5").visible = true;
    }
    private void CerrarMailBox()
    {
        interfazPrincipal.Q<VisualElement>("MailBox").visible = false;
        interfazPrincipal.Q<VisualElement>("BandejaDeEntrada").visible = false;
        interfazPrincipal.Q<VisualElement>("Botonera").visible = false;
        interfazPrincipal.Q<VisualElement>("CintaDeOpciones").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaMail_1").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaMail_2").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaMail_3").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaMail_4").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaMail_5").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_2").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_3").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_4").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_5").visible = false;
        interfazPrincipal.Q<Label>("lblLinkSospechoso").visible = false;
        interfazPrincipal.Q<Label>("lblOrigenSospechoso").visible = false;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_1").visible = false;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_2").visible = false;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_3").visible = false;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_4").visible = false;
        interfazPrincipal.Q<Button>("btnMailAsuntoMail_5").visible = false;

    }

    private void AbrirChat()
    {
        var chat = interfazPrincipal.Q<VisualElement>("AppChat");
        chat.visible = true;
        foreach (var item in chat.Children())
        {
            item.visible = true;
        }

    }

    

    private void MostrarMail1(string mail)
    {
        interfazPrincipal.Q<VisualElement>(mail).visible = true;
        interfazPrincipal.Q<VisualElement>("VentanaReporte").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_2").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_3").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_4").visible = false;
        interfazPrincipal.Q<VisualElement>("VentanaReporte_5").visible = false;

    }

    private void MostrarMensajes(string label)
    {
        interfazPrincipal.Q(label).visible = true;
    }

    private void MostrarVentana(string ventana)
    {
        interfazPrincipal.Q<VisualElement>(ventana).visible = true;

    }

    private void CerrarVentana(string ventana)
    {
        interfazPrincipal.Q<VisualElement>(ventana).visible = false;
    }

    private void ReporteCorrecto(string ventanaMail, string ventanaReporte, string btnAsuntoMail, string lblsospechosa_1, string lblsospechosa_2)
    {
        
        interfazPrincipal.Q<VisualElement>(ventanaMail).visible = false;
        interfazPrincipal.Q<VisualElement>(ventanaReporte).visible = false;
        interfazPrincipal.Q<Label>(lblsospechosa_1).visible = false;
        interfazPrincipal.Q<Label>(lblsospechosa_2).visible = false;
        var btn = interfazPrincipal.Q<Button>(btnAsuntoMail);
        btn.SetEnabled(false);
        btn.style.color = new StyleColor(Color.gray);

    }

    private void ReporteIncorrecto(string labelFallo)
    {
        interfazPrincipal.Q<Label>(labelFallo).visible = true;
        btnCerrarFalloReporte = interfazPrincipal.Q<Button>("btnCerrarFalloReporte");
        btnCerrarFalloReporte.clickable.clicked += () => { CerrarVentana("VentanaReporte"); 
                                              interfazPrincipal.Q<Label>(labelFallo).visible = false; 
                                            };

    }


    void OnEnviarButtonClick()
    {
    

    }
    void OnEliminarButtonClick()
    {
       



    }
    void TerminarJuego()
    {
        
    }




















    void EnviarCorreo(VisualElement ventana)
    {
        // Aqu� puedes implementar la l�gica para enviar el correo basado en la ventana seleccionada.
        // Por ejemplo, obtener el contenido del correo y realizar la acci�n correspondiente.
        Debug.Log("Enviando correo: " + ventana.name);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {

    }
    public void OnPointerExit(PointerEventData eventData)
    {

    }






}
